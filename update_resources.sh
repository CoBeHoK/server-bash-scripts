#!/bin/bash
function process_dir {
for theme_path in $1/frontend/themes/*
do
  if grep -q "node-sass" $theme_path/resources/package.json;
  then
      echo Node-sass exists in $theme_path/resources/package.json >&2
      cd $theme_path/resources
      if npm rebuild node-sass
      then
          return 0
      else
          echo fail to execute \'npm rebuild node-sass\' in $theme_path/resources/ >&2
          return 1
      fi
  else
      echo No Node-sass in $theme_path/resources/package.json >&2
  fi
done
}
if [ -n "$1" ]
then
    process_dir /var/www/$1 || echo Processing interrupted due to error; exit 1;
else
    declare -a blacklist=(uk-brandsaver-net uk-discountminds-net)
    declare -A map_blacklist
    for key in "${!blacklist[@]}"     # expand the array indexes to a list of words
    do
      map_blacklist[${blacklist[$key]}]="$key"  # exchange the value ${a[$key]} with the index $key
    done
    for site_path in /var/www/*
    do
      if [ -d "$site_path" ]
      then
         dir_name=$(basename $site_path)
         if [[ -n "${map_blacklist[$dir_name]}" ]]
         then
             echo "$site_path is in blacklist, ignoring"
             continue
         fi
         process_dir $site_path || echo Processing interrupted due to error; exit 1;
      fi
    done
fi
exit 0